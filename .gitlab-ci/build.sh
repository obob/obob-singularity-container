#!/bin/bash
mkdir images

echo $CI_REGISTRY_IMAGE

for def_file in container/*.def
do
    b_name="$(basename $def_file .def)"
    singularity build images/$b_name.sif $def_file
    singularity push --docker-username "${CI_REGISTRY_USER}" --docker-password "${CI_REGISTRY_PASSWORD}" images/$b_name.sif oras://"${CI_REGISTRY_IMAGE}"/"${CI_PROJECT_NAME}":"${CI_COMMIT_REF_SLUG}"
done